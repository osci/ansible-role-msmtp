import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_mda(host):
    host.run_expect(
        [0],
        'echo "From: test@localhost\nTo:root@localhost\nSubject: test\n\ntest" | sendmail -it',
    )
